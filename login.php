<!DOCTYPE html>
<html>

<head>
    <title>Đăng nhập thành viên</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="include/style/login.css" rel="stylesheet" type="text/css">
    <link href="include/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="container">
        <h1 class="welcome text-center">Shoe Store</h1>
        <div class="card card-container">
            <h2 class='login_title text-center'>Đăng nhập</h2>
            <hr>
            <?php
//Khởi động session
session_start();


//Kiểm tra nếu đã đăng nhập thì quay về trang chủ quản trị
if(isset($_SESSION['user']))
{
	
	header('location:index.php');
	
}


//require các file cần thiết
require_once('lib/db_connect.php');

require_once('lib/user.php');


//kiểm tra dữ liệu từ form
$error = false;

$bool=isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['password']) && !empty($_POST['password']);

if($bool)
{
	
	//l	ấy username và password từ form
	$username = $_POST['username'];
	
	$password = $_POST['password'];
	
	
	//k	iểm tra tồn tại của thành viên
	$user = login($conn,$username);
	
	
	if($user && $user['password']===$password)
	{
		
		//t		hành công thì tạo session lưu đăng nhập
		$_SESSION['user']= $user;
		
		
		//c		huyển hướng về trang chủ quản trị
		header('location:index.php');
		
	}
	else
	{
		
		//t		hất bại thì bật cờ lỗi
		$error=true;
		
	}
	
	
}

if($error)
{
	
	echo '<div class="alert alert-warning" role="alert">Sai tên đăng nhập hoặc mật khẩu!</div>';
	
}

?>
                <form class="form-signin" method="post">
                    <span id="reauth-email" class="reauth-email"></span>
                    <p class="input_title">Tên đăng nhập</p>
                    <input type="text" id="inputEmail" class="login_box" name="username" placeholder="Tên đăng nhập" required autofocus>
                    <p class="input_title">Mật khẩu</p>
                    <input type="password" id="inputPassword" class="login_box" name="password" placeholder="******" required>
                    <button class="btn btn-lg btn-primary" name="btn_submit" type="submit">Đăng Nhập</button>
                </form>
                <!-- /form -->
        </div>
        <!-- /card-container -->
        <h4 class="welcome text-center">&copy; 2017 ShoeStore.com</h4>
    </div>
    <!-- /container -->

</body>

</html>
