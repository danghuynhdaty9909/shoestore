-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: shoestore
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `fullname` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idadmin`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='bang luu username va password cho admin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'y','1','y',1),(2,'trang','1','trang',1),(3,'thuong','1','thuong',1);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitiethoadon`
--

DROP TABLE IF EXISTS `chitiethoadon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitiethoadon` (
  `idchitiethoadon` int(11) NOT NULL AUTO_INCREMENT,
  `idgiohang` int(11) NOT NULL,
  `masanpham` int(11) NOT NULL,
  `soluong` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idchitiethoadon`),
  KEY `fk_chitiethoadon_sanpham1_idx` (`masanpham`),
  KEY `fk_chitiethoadon_giohang1_idx` (`idgiohang`),
  CONSTRAINT `fk_chitiethoadon_giohang1` FOREIGN KEY (`idgiohang`) REFERENCES `giohang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_chitiethoadon_sanpham1` FOREIGN KEY (`masanpham`) REFERENCES `sanpham` (`masanpham`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitiethoadon`
--

LOCK TABLES `chitiethoadon` WRITE;
/*!40000 ALTER TABLE `chitiethoadon` DISABLE KEYS */;
/*!40000 ALTER TABLE `chitiethoadon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `giohang`
--

DROP TABLE IF EXISTS `giohang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giohang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL DEFAULT '1' COMMENT 'user có id là 1 là user null không có thông tin dành cho khách hàng không là thành viên',
  `tenkhachang` varchar(45) NOT NULL,
  `diachigiaohang` varchar(45) DEFAULT NULL,
  `ghichu` varchar(200) DEFAULT NULL,
  `ngaytao` date DEFAULT NULL,
  `ngaythaydoi` date DEFAULT NULL,
  `giatri` int(11) DEFAULT NULL,
  `tinhtrang` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_giohang_user_idx` (`iduser`),
  CONSTRAINT `fk_giohang_user` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giohang`
--

LOCK TABLES `giohang` WRITE;
/*!40000 ALTER TABLE `giohang` DISABLE KEYS */;
/*!40000 ALTER TABLE `giohang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hangsanxuat`
--

DROP TABLE IF EXISTS `hangsanxuat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hangsanxuat` (
  `idhangsanxuat` int(11) NOT NULL AUTO_INCREMENT,
  `tenhangsanxuat` varchar(45) DEFAULT NULL,
  `diachi` varchar(100) DEFAULT NULL,
  `sodienthoai` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`idhangsanxuat`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hangsanxuat`
--

LOCK TABLES `hangsanxuat` WRITE;
/*!40000 ALTER TABLE `hangsanxuat` DISABLE KEYS */;
INSERT INTO `hangsanxuat` VALUES (1,'Công ty NIKE ','Mỹ','0982330421'),(2,'Công ty ADIDAS','Đức','01234567891'),(3,'Công ty PUMA','Mỹ','01245674321'),(4,'Công ty CONVERSE','Anh','01564789237'),(5,'Công ty BITI\'S','Việt Nam','0163832827');
/*!40000 ALTER TABLE `hangsanxuat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hinhanh`
--

DROP TABLE IF EXISTS `hinhanh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hinhanh` (
  `idhinhanh` int(11) NOT NULL AUTO_INCREMENT,
  `masanpham` int(11) DEFAULT NULL,
  `link` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idhinhanh`),
  KEY `fk_hinhanh_sanpham_idx` (`masanpham`),
  CONSTRAINT `fk_hinhanh_sanpham` FOREIGN KEY (`masanpham`) REFERENCES `sanpham` (`masanpham`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hinhanh`
--

LOCK TABLES `hinhanh` WRITE;
/*!40000 ALTER TABLE `hinhanh` DISABLE KEYS */;
INSERT INTO `hinhanh` VALUES (1,1,'image/sanpham/1/2.jpg'),(2,1,'image/sanpham/1/3.jpg'),(3,1,'image/sanpham/1/4.jpg'),(4,1,'image/sanpham/1/5.jpg'),(5,2,'image/sanpham/2/2.jpg'),(6,2,'image/sanpham/2/3.jpg'),(7,2,'image/sanpham/2/4.jpg'),(8,2,'image/sanpham/2/5.jpg'),(9,3,'image/sanpham/3/2.jpg'),(10,3,'image/sanpham/3/3.jpg'),(11,3,'image/sanpham/3/4.jpg'),(12,3,'image/sanpham/3/5.jpg'),(13,3,'image/sanpham/3/6.jpg'),(14,4,'image/sanpham/4/2.jpg'),(15,4,'image/sanpham/4/3.jpg'),(16,4,'image/sanpham/4/4.jpg'),(17,4,'image/sanpham/4/5.jpg'),(18,5,'image/sanpham/5/2.jpg'),(19,5,'image/sanpham/5/3.jpg'),(20,5,'image/sanpham/5/4.jpg'),(21,6,'image/sanpham/6/2.jpg'),(22,6,'image/sanpham/6/3.jpg'),(23,6,'image/sanpham/6/4.jpg'),(24,6,'image/sanpham/6/5.jpg'),(25,7,'image/sanpham/7/2.jpg'),(26,7,'image/sanpham/7/3.jpg'),(27,7,'image/sanpham/7/4.jpg'),(28,7,'image/sanpham/7/5.jpg'),(29,8,'image/sanpham/8/2.jpg'),(30,8,'image/sanpham/8/3.jpg'),(31,8,'image/sanpham/8/4.jpg'),(32,8,'image/sanpham/8/5.jpg'),(36,9,'image/sanpham/9/2.jpg'),(37,9,'image/sanpham/9/3.jpg'),(38,9,'image/sanpham/9/4.jpg'),(39,10,'image/sanpham/10/2.jpg'),(40,10,'image/sanpham/10/3.jpg'),(45,31,'image/sanpham/31/2.jpg'),(46,31,'image/sanpham/31/3.jpg'),(47,31,'image/sanpham/31/4.jpg'),(48,31,'image/sanpham/31/5.jpg');
/*!40000 ALTER TABLE `hinhanh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loaisanpham`
--

DROP TABLE IF EXISTS `loaisanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loaisanpham` (
  `maloaisanpham` int(11) NOT NULL AUTO_INCREMENT,
  `tenloaisanpham` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`maloaisanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loaisanpham`
--

LOCK TABLES `loaisanpham` WRITE;
/*!40000 ALTER TABLE `loaisanpham` DISABLE KEYS */;
INSERT INTO `loaisanpham` VALUES (1,'giày thể thao nam'),(2,'giày thể thao nữ'),(3,'giày thời trang nam'),(4,'giày thời trang nữ'),(5,'giày BITI\'S Hunter');
/*!40000 ALTER TABLE `loaisanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sanpham`
--

DROP TABLE IF EXISTS `sanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sanpham` (
  `masanpham` int(11) NOT NULL AUTO_INCREMENT,
  `maloaisanpham` int(11) NOT NULL,
  `idhangsanxuat` int(11) NOT NULL,
  `tensanpham` varchar(100) DEFAULT NULL,
  `giagoc` int(11) DEFAULT NULL,
  `giagiam` int(11) DEFAULT NULL,
  `giamgia` double DEFAULT '0',
  `soluong` int(11) DEFAULT NULL,
  `chitiet` text,
  `gioitinh` tinyint(1) DEFAULT '1',
  `hinhanh` varchar(45) DEFAULT NULL,
  `trangthai` tinyint(1) DEFAULT '1' COMMENT '1 là còn sử dụng\n0 là hết sử dụng\n',
  PRIMARY KEY (`masanpham`),
  KEY `fk_sanpham_loaisanpham1_idx` (`maloaisanpham`),
  KEY `fk_sanpham_hangsanxuat1_idx` (`idhangsanxuat`),
  CONSTRAINT `fk_sanpham_hangsanxuat1` FOREIGN KEY (`idhangsanxuat`) REFERENCES `hangsanxuat` (`idhangsanxuat`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sanpham_loaisanpham1` FOREIGN KEY (`maloaisanpham`) REFERENCES `loaisanpham` (`maloaisanpham`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sanpham`
--

LOCK TABLES `sanpham` WRITE;
/*!40000 ALTER TABLE `sanpham` DISABLE KEYS */;
INSERT INTO `sanpham` VALUES (1,1,5,'Giày thể thao nam Biti’s Hunter LIMITED EDITION – Hunter BLACK',395000,NULL,0.1,5,'Giày thể thao nam Biti’s Hunter LIMITED EDITION – Hunter BLACK màu đen có kiểu dáng được thiết kế hiện đại và năng động. Được tạo nên từ chất liệu lưới cao cấp giúp đôi chân luôn thoáng mát, mẫu giày này còn có phần đế phylon siêu nhẹ giúp bạn thoải mái vận động trong thời gian dài mà không sợ mỏi chân, cực kì thích hợp với những hoạt động thể thao hay ngoài trời.Chất liệu lưới Air Mesh cao cấp, thoáng mát',1,'image/sanpham/1/1.jpg',1),(2,1,5,'GIÀY THỂ THAO NAM BITI’S CAO CẤP LƯỚI AIR MESH MÀU CAM',650000,NULL,0,5,'Giày thể thao nam Biti’s cao cấp lưới Air Mesh màu cam của dòng Biti’s Hunter là một sản phẩm luôn nằm trong top được ưa chuộng nhất. Với sắc cam phối đen cá tính, sang trọng cùng với kiểu dáng thể thao phóng khoáng, đôi giày chính là một cách để bạn thể hiện cá tính của mình. Tính năng nổi bật: Màu cam kết hợp với màu đen nổi bật, Chất liệu lưới Air Mesh thông thoáng, Đế cao su có rãnh chống trơn trượt, In logo trên mặt lót và sau gót',1,'image/sanpham/2/1.jpg',1),(3,1,5,'GIÀY THỂ THAO NAM BITI’S CAO CẤP LƯỚI AIR MESH_RED',450000,NULL,0.1,3,'Giày thể thao nam Biti’s Hunter LIMITED EDITION – Hunter BLACK màu đen có kiểu dáng được thiết kế hiện đại và năng động. Được tạo nên từ chất liệu lưới cao cấp giúp đôi chân luôn thoáng mát, mẫu giày này còn có phần đế phylon siêu nhẹ giúp bạn thoải mái vận động trong thời gian dài mà không sợ mỏi chân, cực kì thích hợp với những hoạt động thể thao hay ngoài trời.Chất liệu lưới Air Mesh cao cấp, thoáng mát',1,'image/sanpham/3/1.jpg',1),(4,1,5,'Giày thể thao nam Biti’s Hunter LIMITED EDITION – Hunter BLUE',500000,NULL,0,5,'Giày thể thao cao cấp nam Hunter Biti\'s được thiết kể kiểu dáng năng động, trẻ trung với chất liệu vượt trội. Với đế phylon cao cấp đàn hồi tốt, Hunter đảm bảo đem đến cảm giác êm nhẹ, lướt đi trong từng bước chuyển động. Đế tiếp đất từ chất liệu cao su cao cấp được thiết kế rãnh ngang tăng độ bám khi di chuyển trên mọi địa hình. Lớp lưới Air Mesh thông thoáng và siêu nhẹ, tạo cảm giác mát mẻ cho đôi chân và ngăn chặn tình trạng ẩm ướt khó chịu khi hoạt động trong thời gian dài',1,'image/sanpham/4/1.jpg',1),(5,1,5,'Giày thể thao nam Biti’s Hunter LIMITED EDITION – Hunter GREY',590000,NULL,0,5,'Giày Thể Thao Biti\'s Cao Cấp Nam - HUNTER - DSM062133XAM có thiết kế theo phong cách thể thao, đường phố (street style), phóng khoáng cho bạn sự tự do khi thể hiện cá tính nổi bật của chính mình. Màu xám cá tính góp phần tạo điểm nhấn hoàn hảo cho bộ trang phục của bạn. Ngoài ra, thiết kế thoát ẩm hiệu quả cùng đế giày với các rãnh chống trơn làm tăng độ bám, an toàn hơn và giúp đôi chân thoải mái mỗi khi di chuyển.',1,'image/sanpham/5/1.jpg',1),(6,1,5,'Giày thể thao nữ Biti’s Hunter LIMITED EDITION – Hunter PINK GREY',600000,NULL,0.1,5,'Giày thể thao nữ Biti’s Hunter LIMITED EDITION –  có kiểu dáng được thiết kế hiện đại và năng động. Được tạo nên từ chất liệu lưới cao cấp giúp đôi chân luôn thoáng mát, mẫu giày này còn có phần đế phylon siêu nhẹ giúp bạn thoải mái vận động trong thời gian dài mà không sợ mỏi chân, cực kì thích hợp với những hoạt động thể thao hay ngoài trời.Chất liệu lưới Air Mesh cao cấp, thoáng mát',1,'image/sanpham/6/1.jpg',1),(7,1,5,'Giày thể thao nữ Biti’s Hunter LIMITED EDITION – Hunter PINK green',590000,NULL,0.1,7,'Giày thể thao nữ Biti’s Hunter LIMITED EDITION –  có kiểu dáng được thiết kế hiện đại và năng động. Được tạo nên từ chất liệu lưới cao cấp giúp đôi chân luôn thoáng mát, mẫu giày này còn có phần đế phylon siêu nhẹ giúp bạn thoải mái vận động trong thời gian dài mà không sợ mỏi chân, cực kì thích hợp với những hoạt động thể thao hay ngoài trời.Chất liệu lưới Air Mesh cao cấp, thoáng mát',1,'image/sanpham/7/1.jpg',1),(8,1,5,'Giày thể thao nam Biti\'s Hunter Liteknit Đỏ  ',650000,NULL,0.1,10,'Sau cơn sốt mang tên Biti\'s Hunter, thương hiệu Biti\'s tiếp tục trình làng phiên bản nâng cấp với tên gọi Biti\'s Hunter Liteknit. Sở hữu nhiều cải tiến về công nghệ lẫn thiết kế, những đôi Biti\'s Hunter Liteknit hứa hẹn sẽ gặt hái nhiều thành công. So với sản phẩm trước, phiên bản mới có sự thay đổi ở chất liệu quai dệt. Áp dụng công nghệ quai dệt mới có tên gọi Liteknit, có lỗ thoát khí giúp tạo sự thông thoáng. Bên cạnh đó, giày có kiểu dáng và cấu trúc mới đáp ứng các yêu cầu năng động giới trẻ, phù hợp với các hoạt động thể thao và sử dụng thường nhật. Phần đế từ chất liệu phylon siêu nhẹ, kết hợp đế tiếp đất cao su tạo sự ma sát tốt, chống trơn trượt, giúp người mang di chuyển thoải mái và dễ dàng.',1,'image/sanpham/8/1.jpg',1),(9,1,5,'Giày thể thao nam Biti\'s Hunter Liteknit Xanh minơ',650000,NULL,0.1,10,'Sau cơn sốt mang tên Biti\'s Hunter, thương hiệu Biti\'s tiếp tục trình làng phiên bản nâng cấp với tên gọi Biti\'s Hunter Liteknit. Sở hữu nhiều cải tiến về công nghệ lẫn thiết kế, những đôi Biti\'s Hunter Liteknit hứa hẹn sẽ gặt hái nhiều thành công. So với sản phẩm trước, phiên bản mới có sự thay đổi ở chất liệu quai dệt. Áp dụng công nghệ quai dệt mới có tên gọi Liteknit, có lỗ thoát khí giúp tạo sự thông thoáng. Bên cạnh đó, giày có kiểu dáng và cấu trúc mới đáp ứng các yêu cầu năng động giới trẻ, phù hợp với các hoạt động thể thao và sử dụng thường nhật. Phần đế từ chất liệu phylon siêu nhẹ, kết hợp đế tiếp đất cao su tạo sự ma sát tốt, chống trơn trượt, giúp người mang di chuyển thoải mái và dễ dàng.',1,'image/sanpham/9/1.jpg',1),(10,1,5,'Giày thể thao nam Biti\'s Hunter Liteknit Xanh dương',650000,NULL,0.1,5,'Sau cơn sốt mang tên Biti\'s Hunter, thương hiệu Biti\'s tiếp tục trình làng phiên bản nâng cấp với tên gọi Biti\'s Hunter Liteknit. Sở hữu nhiều cải tiến về công nghệ lẫn thiết kế, những đôi Biti\'s Hunter Liteknit hứa hẹn sẽ gặt hái nhiều thành công. So với sản phẩm trước, phiên bản mới có sự thay đổi ở chất liệu quai dệt. Áp dụng công nghệ quai dệt mới có tên gọi Liteknit, có lỗ thoát khí giúp tạo sự thông thoáng. Bên cạnh đó, giày có kiểu dáng và cấu trúc mới đáp ứng các yêu cầu năng động giới trẻ, phù hợp với các hoạt động thể thao và sử dụng thường nhật. Phần đế từ chất liệu phylon siêu nhẹ, kết hợp đế tiếp đất cao su tạo sự ma sát tốt, chống trơn trượt, giúp người mang di chuyển thoải mái và dễ dàng.',1,'image/sanpham/10/1.jpg',1),(31,1,3,'GIÀY PUMA SUEDE LEATHER NAM - ĐEN',2290000,NULL,0.1,100,'Giày Puma Suede Leather được thiết kế đẹp mắt, phổ biến từ lâu mà có thể bắt gặp ở sân bóng rổ từ những năm 60, sàn hiphop những năm 90 và nay là trên những vỉa hè phố. Đôi giày sử dụng chất liệu da lộn mềm mịn và có kiểu dáng thể thao, được đánh giá là kiểu giày sneaker mang tính biểu tượng của thương hiệu Puma được duy trì đến ngày nay. ',1,'image/sanpham/31/1.jpg',1),(32,1,3,'GIÀY PUMA G. VILAS 2 CORE NAM - TRẮNG ĐỎ',2120000,NULL,0.15,100,'Giày Puma G. Vilas 2 Core sử dụng chất liệu vải lưới và da tổng hợp phối hợp ăn ý với nhau, cùng với đó là các chi tiết nhấn được làm vô cùng hoàn hảo. Đế giữa của giày êm ái, phần buộc dây chắc chắn đảm bảo độ vừa vặn, mang đến sự thoải mái tối đa cho bàn chân. Ngoài ra, đế giày Puma G. Vilas 2 Core được làm từ cao su, các vân đế cho độ bám cao, thêm vào đó là kết cấu vân hình tròn giúp bàn chân uyển chuyển khi đổi hướng.',1,'image/sanpham/32/1.jpg',1),(33,1,3,'GIÀY PUMA SUEDE CLASSIC DEBOSSED NAM - XANH',1550000,NULL,0,200,'Giày PUMA Suede Classic Debossed được thiết kế đẹp mắt, phổ biến từ lâu mà có thể bắt gặp ở sân bóng rổ từ những năm 60, sàn hiphop những năm 90 và nay là trên những vỉa hè phố. Đôi giày sử dụng chất liệu da lộn mềm mịn và có kiểu dáng thể thao, được đánh giá là kiểu giày sneaker mang tính biểu tượng của thương hiệu Puma được duy trì đến ngày nay. ',1,'image/sanpham/33/1.jpg',1),(34,1,3,'GIÀY PUMA SMASH BUCK MONO NAM',2280000,NULL,0.2,150,'Giày Puma Smash Buck Mono được mang thiết kế của kiểu giày quần vợt những năm 1970 do đó đôi giày có vẻ đẹp rất cổ điển. Tuy nhiên, không thể phủ nhận rằng phối màu đơn sắc vô cùng ấn tượng đã khiến giày Puma Smash Buck Mono trở nên phong cách và hiện đại hơn.',1,'image/sanpham/34/1.jpg',1);
/*!40000 ALTER TABLE `sanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `size` (
  `idsize` int(11) NOT NULL AUTO_INCREMENT,
  `masanpham` int(11) NOT NULL,
  `tensize` varchar(45) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsize`,`masanpham`),
  KEY `fk_size_sanpham_idx` (`masanpham`),
  CONSTRAINT `fk_size_sanpham` FOREIGN KEY (`masanpham`) REFERENCES `sanpham` (`masanpham`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size`
--

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` VALUES (1,1,'40',100);
/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `fullname` varchar(45) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1 là active\n0 là disable',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ykienphanhoi`
--

DROP TABLE IF EXISTS `ykienphanhoi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ykienphanhoi` (
  `idykienphanhoi` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL DEFAULT '1',
  `noidung` text,
  `ngaytao` date DEFAULT NULL,
  PRIMARY KEY (`idykienphanhoi`),
  KEY `fk_ykienphanhoi_user1_idx` (`iduser`),
  CONSTRAINT `fk_ykienphanhoi_user1` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ykienphanhoi`
--

LOCK TABLES `ykienphanhoi` WRITE;
/*!40000 ALTER TABLE `ykienphanhoi` DISABLE KEYS */;
/*!40000 ALTER TABLE `ykienphanhoi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-18  2:26:02
