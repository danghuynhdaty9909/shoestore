<!DOCTYPE html>
<html>

<head>
    <title>Đăng ký thành viên</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="include/style/signin.css" rel="stylesheet" type="text/css">
    <link href="include/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="container">
        <h1 class="welcome text-center">Shoe Store</h1>
        <div class="card col-md-8">
            <h2 class='login_title text-center'>Đăng ký thành viên</h2>
            <hr>
            <?php
            //khởi tạo session
            session_start();
            
            //require các file cần thiết
            require_once('lib/db_connect.php');

            require_once('lib/user.php');
            
            //kiểm tra điều kiện để insert database
            /*
            hàm isset dùng để kiểm tra biến có tồn tại hay không
            nếu có thì gán giá trị cho biến còn nếu chưa thì gán bằng ''
            */
            $username = isset($_POST['username']) ? $_POST['username'] : '';
            $password = isset($_POST['password']) ? $_POST['password'] : '';
            $fullname = isset($_POST['fullname']) ? $_POST['fullname'] : '';
            $address = isset($_POST['address']) ? $_POST['address'] : '';
            $sex = isset($_POST['sex']) ? $_POST['sex'] : '';
            $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
            $email = isset($_POST['email']) ? $_POST['email'] : '';
            
            if(isset($_POST['btn_submit']))
            {
                if(sigin($conn,$username,$password,$fullname,$sex,$address,$phone,$email))
                {
                    header('location:index.php');
                }else
                {
                   echo '<div class="alert alert-warning" role="alert" id="alert">Tên đăng nhập đã tồn tại!</div>';; 
                }
            }
            
            ?>

                <script type="text/javascript">
                    function validateForm() {
                        var password = document.getElementById('password').value;
                        var re_password = document.getElementById('re_password').value;
                        if (password != re_password) {
                            //document.getElementById('alert').innerHTML = "";
                            //<p class="alert danger-alert" role="alert" id="alert"></p>
                            alert('Xác nhận mật khẩu không đúng!');
                            return false;
                        }
                    }

                </script>
                <form class="form-signin" method="post" onsubmit="return validateForm()">
                    <div class=" col-md-6">
                        <p class="input_title">Tên đăng nhập</p>
                        <input type="text" id="username" class="login_box" name="username" placeholder="Tên đăng nhập" required autofocus>
                        <p class="input_title">Mật khẩu</p>
                        <input type="password" id="password" class="login_box" name="password" placeholder="*******" required>
                        <p class="input_title ">Email</p>
                        <input type="email" id="email" class="login_box" name="email" placeholder="Email" required>
                        <p class="input_title">Địa chỉ</p>
                        <input type="text" id="address" class="login_box" name="address" placeholder="Địa chỉ" required>
                    </div>
                    <div class="col-md-6">
                        <p class="input_title ">Tên hiển thị</p>
                        <input type="text" id="fullname" class="login_box" name="fullname" placeholder="Tên hiển thị" required>
                        <p class="input_title ">Xác nhận mật khẩu</p>
                        <input type="password" id="re_password" class="login_box" name="re_password" placeholder="******" required>
                        <p class="input_title">Giới tính</p>
                        <select id="sex" class="login_box" name="sex">
                            <option value="Nam">Nam</option>
                            <option value="Nu">Nữ</option>
                        </select>
                        <p class="input_title">Số điện thoại</p>
                        <input type="text" id="phone" class="login_box" name="phone" placeholder="Số điện thoại" required>
                    </div>
                    <button class="btn btn-lg btn-primary" name="btn_submit" type="submit">Đăng Ký</button>
                </form>
                <!-- /form -->
        </div>
        <!-- /card-container -->

    </div>
    <!-- /container -->
    <h4 class="welcome text-center ">&copy; 2017 ShoeStore.com</h4>
</body>

</html>
