<div id="page-wrapper">
        <!-- title -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Quản lý thành viên</h1>
            </div>
        </div>
        <!-- /title -->

        <!-- table -->
        <div class="row">
            <div class="col-md-12">
                <!--đơn hàng mới nhat-->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Danh sách thành viên
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body" style="padding-bottom:0;">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>Mã thành viên</th>
                                    <th>Tên đăng nhập</th>
                                    <th>Tên đầy đủ</th>
                                    <th>Giới tính</th>
                                    <th>Trạng thái</th>
                                    <th>Hành động</th>

                                </thead>
                                <tbody>
                                <?php
                                
                                function user($conn){
                                    //tìm tổng số record trong bảng product
                                    $result = select_db($conn,"select count(*) as total from `user`");
                                    $total_record = $result['total'];

                                    //tìm limit và recent page
                                    $limit = 10;
                                    $current_page = isset($_GET['p'])? $_GET['p'] : 1;

                                    //tính toán total page 
                                    $total_page = ceil($total_record / $limit);

                                    // Giới hạn current_page trong khoảng 1 đến total_page
                                    if ($current_page > $total_page){
                                        $current_page = $total_page;
                                    }
                                    else if ($current_page < 1){
                                        $current_page = 1;
                                    }
                                    
                                    // Tìm Start
                                    $start = ($current_page - 1) * $limit;
                                    
                                    // BƯỚC 5: TRUY VẤN LẤY DANH SÁCH TIN TỨC
                                    // Có limit và start rồi thì truy vấn CSDL lấy danh sách tin tức
                                    $result = mysqli_query($conn, "SELECT * FROM `user` LIMIT $start, $limit");

                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo '<tr>';                                
                                       echo "<td>".$row['iduser']."</td>";
                                       echo "<td>".$row['username']."</td>";

                                       
                                       echo "<td>".$row['fullname']."</td>";
                                       //code xử lý giới tính
                                       if($row['sex']==1)
                                      {
                                        echo "<td>Nam</td>";
                                      }else
                                      {
                                        echo "<td>Nữ</td>";
                                      }
                                      //code xử lý trạng thái
                                      if($row['status']==1)
                                      {
                                        echo "<td>hoạt động</td>";
                                      }else
                                      {
                                        echo "<td>không hoạt động</td>";
                                      }
                                      echo '<td>
                                        <a href="index.php?t=productdetail&id='.$row['iduser'].'"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                        <a href="index.php?t=editproduct&id='.$row['iduser'].'"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                        <a href="index.php?t=deleteproduct&id='.$row['iduser'].'"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                    </td>';
                                    echo '</tr>';
                                    }
                                }
                                
                                user($conn);

                                

                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

        <?php
            //include file pagination
            include('include/pagination.php');
            pagination($conn,'user','user');
         ?>
</div>