<?php

//$table là table cần phân trang. $table là 1 bảng trong csdl
//$ppp là số sản phẩm mỗi trang
    function pagination($conn,$table,$t){
        echo '<div class="row"><div class="col-lg-12"><nav aria-label="Page navigation" class="navbar-right"><ul class="pagination">';
        //code xử lý
        //tìm tổng số record trong bảng product
        $result = mysqli_query($conn,"select count(*) as total from `$table`");
        $row=mysqli_fetch_assoc($result);
        $total_record = $row['total'];

        //tìm limit và recent page
       
        $current_page = isset($_GET['p'])? $_GET['p'] : 1;
        $limit = 10;
        //tính toán total page 
        $total_page = ceil($total_record / $limit);

        // nếu current_page > 1 và total_page > 1 mới hiển thị nút prev
        if ($current_page > 1 && $total_page > 1){
            echo '<li><a class="fa fa-backward" href="index.php?t='.$t.'&p='.($current_page-1).'"></a></li>';
        }                           

        // Lặp khoảng giữa
        for ($i = 1; $i <= $total_page; $i++){
            // Nếu là trang hiện tại thì hiển thị thẻ span
            // ngược lại hiển thị thẻ a
            if ($i == $current_page){
                echo '<li class="active"><span>'.$i.'</span></li>';
            }
            else{
                echo '<li><a href="index.php?t='.$t.'&p='.$i.'">'.$i.'</a></li>';
            }
        }

        // nếu current_page < $total_page và total_page > 1 mới hiển thị nút prev
        if ($current_page < $total_page && $total_page > 1){
            echo '<li><a class="fa fa-forward" href="index.php?t='.$t.'&p='.($current_page+1).'"></a></li>';
        }
        echo '</ul></nav></div></div>';
    }
    
                   
