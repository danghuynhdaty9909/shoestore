<?php
        //heder file
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Trang chủ quản trị viên </title>

    <!-- Bootstrap Core CSS -->
    <link href="include/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="include/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="include/custom/css/sb-admin-2.css" rel="stylesheet">   

    <!-- Custom Fonts -->
    <link href="include/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>
<body>


    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <a class="navbar-brand" href="index.php" style="color:#337AB7;">  Shoe Store</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            
            <li class="dropdown"> 
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <?php
                    //khởi động session
                    session_start();
                    $admin = $_SESSION['admin'];
                    echo $admin['fullname'];
                    ?>
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> Thông tin tài khoản</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Cài đặt</a>
                    </li>
                    <!--<li class="divider"></li>-->
                    <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Đăng xuất</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <!--.navbar-static-side-->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    
                    <li>
                        <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Bảng điều khiển</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-tags fa-fw"></i> Chi tiết<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="index.php?t=order">Đơn hàng</a>
                            </li>
                             <li>
                                <a href="index.php?t=category">Danh mục sản phẩm</a>
                            </li>
                            <li>
                                <a href="index.php?t=product">Sản phẩm</a>
                            </li>
                           
                            <li>
                                <a href="index.php?t=sale">Khuyến mãi</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="index.php?t=notification"><i class="fa fa-bell fa-fw"></i> Thông báo</a>
                    </li>
                    <li>
                        <a href="index.php?t=user"><i class="fa fa-user-circle fa-fw"></i> Khách hàng</a>
                    </li>
                    
                     <li>
                        <a href="index.php?t=report"><i class="fa fa-reply fa-fw"></i> Khách hàng góp ý</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Công cụ<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="panels-wells.html">Viết Bài</a>
                            </li>
                            <li>
                                <a href="buttons.html">Đăng hình ảnh</a>
                            </li>
                            <li>
                                <a href="notifications.html">Sao lưu dữ liệu</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>