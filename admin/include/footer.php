        <!--footer-->
        <div class="row">
            <p class="text-center">ShoeStore &copy; 2017 Mọi quyền được bảo lưu<br>ShoeStore.com</p>
        </div>
        
    </div>
    <!-- /#page-wrapper -->

    <!-- jQuery -->
    <script src="include/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="include/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="include/metisMenu/metisMenu.min.js"></script>

  

    <!-- Custom Theme JavaScript -->
    <script src="include/custom/js/sb-admin-2.js"></script>

</body>

</html>