<div id="page-wrapper">
    <!-- title -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Ý kiến thành viên</h1>
        </div>
    </div>
    <!-- /title -->

    <!-- table -->
    <div class="row">
        <div class="col-md-12">
            <!--đơn hàng mới nhat-->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Đơn hàng mới nhất
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body" style="padding-bottom:0;">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <th>Mã ý kiến</th>
                                <th>Tên thành viên</th>
                                <th>Nội dung</th>
                                <th>Ngày tạo</th>
                                <th>Hành động</th>
                            </thead>
                            <tbody>
                                <?php
                                
                                function product($conn){
                                    //tìm tổng số record trong bảng product
                                    $result = mysqli_query($conn,"select count(*) as total from `ykienphanhoi`");
                                    $row=mysqli_fetch_assoc($result);
                                    $total_record = $row['total'];

                                    //tìm limit và recent page
                                    $limit = 10;
                                    $current_page = isset($_GET['p'])? $_GET['p'] : 1;

                                    //tính toán total page 
                                    $total_page = ceil($total_record / $limit);

                                    // Giới hạn current_page trong khoảng 1 đến total_page
                                    if ($current_page > $total_page){
                                        $current_page = $total_page;
                                    }
                                    else if ($current_page < 1){
                                        $current_page = 1;
                                    }
                                    
                                    // Tìm Start
                                    $start = ($current_page - 1) * $limit;
                                    
                                    // BƯỚC 5: TRUY VẤN LẤY DANH SÁCH TIN TỨC
                                    // Có limit và start rồi thì truy vấn CSDL lấy danh sách tin tức
                                    $result = mysqli_query($conn, "SELECT * FROM `ykienphanhoi` LIMIT $start, $limit");

                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo '<tr>';
                                       echo "<td>".$row['idykienphanhoi']."</td>";
                                       //code xử lý tên khách hàng
                                       $iduser=$row['iduser'];
                                       if($iduser==0)
                                       {
                                         echo "<td>chưa là thành viên</td>";
                                       }else
                                       {
                                         $user = select_db($conn,"select fullname from `user` where iduser='$iduser'");
                                       echo "<td>".$user['fullname']."</td>";
                                       }
                                      
                                       //code xử lý nội dung
                                       $noidung = substr($row['noidung'],0,30);
                                       echo "<td>".$noidung."...</td>";
                                       
                                       echo "<td>".$row['ngaytao']."</td>";
                                      echo '<td>
                                        <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                    </td>';
                                    echo '</tr>';
                                    }
                                }
                                
                                product($conn);

                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->

    <?php
            //include file pagination
            include('pagination.php');
            pagination($conn,'ykienphanhoi','report');
         ?>
</div>
