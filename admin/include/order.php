<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Đơn hàng</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--đơn hàng mới nhat-->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Đơn hàng mới nhất
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body" style="padding-bottom:0;">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>Mã đơn hàng</th>
                                    <th>Tên khách hàng</th>
                                    <th>Trạng thái</th>
                                    <th>Tổng tiền</th>
                                    <th>Ngày đặt hàng</th>
                                    <th>Ngày sửa đổi</th>
                                    <th>Hành động</th>

                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>dang huynh dat y</td>
                                        <td>Đang thực hiện</td>
                                        <td>1000000 đồng</td>
                                        <td>14/04/2017</td>
                                        <td>14/04/2017</td>
                                        <td>
                                            <a href="#"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pencil btn btn-primary" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->
        
        
        <!--pagination.php -->
        <div class="row">
            <div class="col-lg-12">
                <nav aria-label="Page navigation" class="navbar-right">
                    <ul class="pagination">
                        <li>
                            <a class="fa fa-backward" href="#"></a>
                        </li>
                        <li class="active"><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li><a href="#">8</a></li>
                        <li><a href="#">9</a></li>
                        <li>
                            <a class="fa fa-forward" href="#"></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
</div>