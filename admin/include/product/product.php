<div id="page-wrapper">
        <!-- title -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sản phẩm</h1>
            </div>
        </div>
        <!-- /title -->

        <!-- table -->
        <div class="row">
            <div class="col-md-12">
                <!--đơn hàng mới nhat-->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Danh sách sản phẩm
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body" style="padding-bottom:0;">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>Mã sản phẩm</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Hãng sản xuất</th>
                                    <th>Giá gốc</th>
                                    <th>Trạng thái</th>
                                    <th>Hành động</th>

                                </thead>
                                <tbody>
                                <?php
                                
                                function product($conn){
                                    //tìm tổng số record trong bảng product
                                    $result = select_db($conn,"select count(*) as total from `sanpham`");
                                    $total_record = $result['total'];

                                    //tìm limit và recent page
                                    $limit = 10;
                                    $current_page = isset($_GET['p'])? $_GET['p'] : 1;

                                    //tính toán total page 
                                    $total_page = ceil($total_record / $limit);

                                    // Giới hạn current_page trong khoảng 1 đến total_page
                                    if ($current_page > $total_page){
                                        $current_page = $total_page;
                                    }
                                    else if ($current_page < 1){
                                        $current_page = 1;
                                    }
                                    
                                    // Tìm Start
                                    $start = ($current_page - 1) * $limit;
                                    
                                    // BƯỚC 5: TRUY VẤN LẤY DANH SÁCH TIN TỨC
                                    // Có limit và start rồi thì truy vấn CSDL lấy danh sách tin tức
                                    $result = mysqli_query($conn, "SELECT * FROM `sanpham` LIMIT $start, $limit");

                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo '<tr>';                                
                                       echo "<td>".$row['masanpham']."</td>";
                                       echo "<td>".$row['tensanpham']."</td>";

                                       //code cho hãng sản xuất
                                       $idhangsanxuat = $row['idhangsanxuat'];
                                       
                                       $hangsanxuat = select_db($conn,"select tenhangsanxuat from hangsanxuat where idhangsanxuat='$idhangsanxuat'");
                                       echo "<td>".$hangsanxuat['tenhangsanxuat']."</td>";

                                       echo "<td>".$row['giagoc']."</td>";

                                      if($row['trangthai']==1)
                                      {
                                        echo "<td>còn hàng</td>";
                                      }else
                                      {
                                        echo "<td>hết hàng</td>";
                                      }
                                      echo '<td>
                                        <a href="index.php?t=productdetail&id='.$row['masanpham'].'"><i class="fa fa-eye  btn btn-info" aria-hidden="true"></i></a>
                                        <a href="index.php?t=deleteproduct&id='.$row['masanpham'].'"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i></a>
                                    </td>';
                                    echo '</tr>';
                                    }
                                }
                                
                                product($conn);

                                

                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

        <?php
            //include file pagination
            include('include/pagination.php');
            pagination($conn,'sanpham','product');
         ?>
</div>