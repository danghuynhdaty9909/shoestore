<?php
    //include file kết nối db
       include ('../lib/db_connect.php');
    //include các file giao dien
    include('include/header.php');
 

    //xử lý url

    $t=isset($_GET['t'])?$_GET['t']:"dashboard";//nếu biến $_GET['t'] tồn tại thì gán giá trị cho $p không thì gán bằng null
    
    
    switch($t){
        //trang dashboard
        case "dashboard": include('include/dashboard.php'); break;

        //trang giỏ hàng
        case "order": include('include/order.php'); break;

        //=======================code cho phần sản phẩm================================

        //trang sản phẩm
        case "product":include('include/product/product.php'); break; 

        //trang xem sanpham
        case "productdetail":include('include/product/productdetail.php'); break;  

        //trang thêm sản phẩm
        case "deleteproduct":include('include/product/deleteproduct.php'); break;

        //trang xóa sản phẩm
        case "addproduct":include('include/product/addproduct.php'); break;
        //=======================hết code cho phần sản phẩm================================

        //trang báo cáo
        case "report": include('include/report.php'); break;

        //trang loại sản phẩm
        case "category": include('include/category.php'); break;

        //trang khuyến mãi
        case "sale": include('include/sale.php'); break;

         //=======================code cho phần khách hàng================================
        //trang khách hàng
        case "user": include('include/user/user.php'); break;

        //trang xem thông tin khách hàng
        case "user": include('include/user/user.php'); break;

        //trang sửa  thông tin khách hàng
        case "user": include('include/user/user.php'); break;

        //=======================hết code cho phần khách hàng================================
        //trang thông báo
        case "notification": include('include/notification.php'); break;
        default : include('include/404.php'); break;
    }


    //kết thúc xử lý url

    include('include/footer.php');
