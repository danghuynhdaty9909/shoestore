<!DOCTYPE html>
<html>

<head>
    <title>Đăng nhập quản trị viên</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="include/style/css/login.css" rel="stylesheet" type="text/css">
    <link href="include/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="container">
        <h1 class="welcome text-center">Shoe Store</h1>
        <div class="card card-container">
            <h2 class='login_title text-center'>Đăng nhập</h2>
            <hr>
            <?php
                //Khởi động session
            session_start();

            //Kiểm tra nếu đã đăng nhập thì quay về trang chủ quản trị
            if(isset($_SESSION['admin']))
            {
                header('location:index.php');
            }

            //require các file cần thiết
            require_once('../lib/db_connect.php');
            require_once('../lib/admin.php');

            //kiểm tra dữ liệu từ form
            $error = false;
            $bool=isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['password']) && !empty($_POST['password']);
            if($bool)
            {
                //lấy username và password từ form
                $username = $_POST['username'];
                $password = $_POST['password'];

                //kiểm tra tồn tại của thành viên
                $admin = check_admin($username,$conn);

                if($admin && $admin['password']===$password)
                {
                    //thành công thì tạo session lưu đăng nhập
                    $_SESSION['admin']= $admin;
                    
                    //chuyển hướng về trang chủ quản trị
                    header('location:index.php');
                }else
                {
                    //thất bại thì bật cờ lỗi
                    $error=true;
                }

            }
                    if($error)
                    {
                        echo '<div class="alert alert-warning" role="alert">Sai tên đăng nhập hoặc mật khẩu!</div>';
                    }
                ?>
                <form class="form-signin" method="post">
                    <span id="reauth-email" class="reauth-email"></span>
                    <p class="input_title">Tên đăng nhập</p>
                    <input type="text" id="inputEmail" class="login_box" name="username" placeholder="Tên đăng nhập" required autofocus>
                    <p class="input_title">Mật khẩu</p>
                    <input type="password" id="inputPassword" class="login_box" name="password" placeholder="******" required>
                    <button class="btn btn-lg btn-primary" name="btn_submit" type="submit">Đăng Nhập</button>
                </form>
                <!-- /form -->
        </div>
        <!-- /card-container -->
        <h4 class="welcome text-center">&copy; 2017 ShoeStore.com</h4>
    </div>
    <!-- /container -->

</body>

</html>
